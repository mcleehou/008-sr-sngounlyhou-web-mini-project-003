import React, { useState } from 'react'
import { Row, Col, Card, Button, Spinner } from 'react-bootstrap'
import { strings } from '../localization/language'
import ReactPaginate from 'react-paginate'
import { deleteArticle, fetchAddArticle, fetchAllArticle, fetchAllCategories, onFetchArticle } from '../redux/actions/articleAction'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import { Link } from 'react-router-dom'
import loading from '../util/loading'

export default function MyHome() {

    const [page, setPage] = useState(1)
    const [isLoading, setIsLoading] = useState(false)

    const dispatch = useDispatch()
    const result = useSelector(state => state)
    useEffect(() => {
        dispatch(fetchAllArticle(page))
        dispatch(fetchAllCategories())
        // setIsLoading(false)
    }, [])

    const onPageChange = ({ selected }) => {
        setIsLoading(true)
        dispatch(fetchAllArticle(selected + 1)).then(()=>setIsLoading(false))
    }

    const onDelete = (idx) =>{
        let response = dispatch(deleteArticle(idx))
    }

    return (
        <div>
            <Row>
                <Col sm={3}>
                    <Card>
                        <Card.Img variant="top" src={'../../images/profile.jpg'} />
                        <Card.Body>
                            <Card.Title></Card.Title>
                            <Card.Text>
                                Please Login
                            </Card.Text>
                            <Button variant="primary">Login with Google</Button>
                        </Card.Body>
                    </Card>
                </Col>
                <Col sm={9}>
                    <h2>{strings.category}</h2>
                    { isLoading? loading('bars','blue') : null }
                    {
                        result.category.map((item, index) => (
                            <span key={index}>
                                <Button variant="light">{item.name}</Button> {` `}
                            </span>
                        ))

                    }
                    <Row className="mt-5">
                        {
                            result.article.map((item, index) => (
                                <Col sm={3} key={index}>
                                    <Card className="mb-3">
                                        <Card.Img variant="top" src={item.image} />
                                        <Card.Body>
                                            <Card.Title>{item.title.length > 20 ?
                                                `${item.title.substring(0, 20)}...` : item.title
                                            }</Card.Title>
                                            <Card.Text>
                                                {item.description.length > 100 ?
                                                    `${item.description.substring(0, 100)}...` : item.description
                                                }
                                            </Card.Text>
                                            <Button as={Link} to={`/view/${item._id}`} variant="primary" size="sm">View</Button>{` `}
                                            <Button as={Link} to={`/edit/${item._id}`} variant="warning" size="sm">Edit</Button>{` `}
                                            <Button variant="danger" size="sm"
                                            onClick={()=>onDelete(item._id)}
                                            >Delete</Button>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            ))
                        }

                    </Row>
                    <Row>
                        <ReactPaginate
                            pageCount={result.totalPage}
                            onPageChange={onPageChange}
                            containerClassName="pagination"
                            pageClassName="page-item"
                            pageLinkClassName="page-link"
                            previousClassName="page-item"
                            previousLinkClassName="page-link"
                            nextLinkClassName="page-link"
                            nextClassName="page-item"
                            activeClassName="active"
                        />
                    </Row>
                </Col>
            </Row>


        </div>
    )
}
