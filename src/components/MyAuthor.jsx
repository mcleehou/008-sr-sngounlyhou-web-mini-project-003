import React, { useEffect, useState } from 'react'
import { Row, Col, Form, Button, Table, Toast } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { strings } from '../localization/language'
import { deleteAuthor, fetchAddAuthor, fetchAllAuthor, fetchEditAuthor } from '../redux/actions/articleAction'
import { fetchAuthorById, uploadImage } from '../service/amsService'

export default function MyAuthor() {

    const dispatch = useDispatch()
    const state = useSelector(state => state.author)

    const [name, setName] = useState(" ")
    const [email, setEmail] = useState(" ")
    const [myImage, setMyImage] = useState("")
    const [browsedImage, setBrowsedImage] = useState("")
    const [isSave, setIsSave] = useState(false)
    const [curId, setCurId] = useState("")
    const [index, setIndex] = useState(null)
    const [message, setMessage] = useState("")
    const [toastShow, setToastShow] = useState(false)

    const toggle = () => setToastShow(!toastShow)

    const pattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z]+$/

    useEffect(() => {
        dispatch(fetchAllAuthor())
    }, [])

    let isEmailValid = pattern.test(email)

    useEffect(() => {
        if (name === "" || name === " " || email === "" || email === " " || !isEmailValid)
            setIsSave(false)
        else setIsSave(true)

        if (name === "")
            document.getElementById('name').innerText = "Name Cannot be Empty!"
        else document.getElementById('name').innerText = ""

        if (email === "")
            document.getElementById('email').innerText = "Email Cannot be Empty!"
        else if (email !== " " && !isEmailValid)
            document.getElementById('email').innerText = "Invalid Email!"
        else document.getElementById('email').innerText = ""
    })

    const onDelete = (idx) => {
        let response = dispatch(deleteAuthor(idx))
    }

    const onTextChange = (e) => {
        if (curId !== "") {
            setName(document.getElementById('input-name').value)
            setEmail(document.getElementById('input-email').value)
        }
        if (e.target.name === "name") {
            setName(e.target.value)
            console.log(name)
        }
        else {
            setEmail(e.target.value)
        }
    }

    const onSave = async () => {

        if (curId === "") {
            let upLoaded = myImage && await uploadImage(myImage)
            let tempAuthor = {
                name: name,
                email: email,
                image: upLoaded
            }
            let response = dispatch(fetchAddAuthor(tempAuthor))
            setMessage(response.message)
        }
        else{
            let upLoaded = myImage && await uploadImage(myImage)
            let tempAuthor = {
                name: name,
                email: email,
                image: upLoaded? upLoaded : browsedImage
            }
            let response = dispatch(fetchEditAuthor(tempAuthor,index,curId))
            console.log("Response:",response)
            setMessage(response.message)
        }
        document.getElementById('input-name').value = ""
        document.getElementById('input-email').value = ""
        setBrowsedImage("")
        setCurId("")
        setMyImage("")
        setIsSave(false)
        setToastShow(true)
    }

    const onSelect = (item,index) => {
        setBrowsedImage(item.image)
        setIndex(index)
        document.getElementById('input-name').value = item.name
        document.getElementById('input-email').value = item.email
        setCurId(item._id)
        console.log("OnSelect", browsedImage)
        console.log("My Image: ", myImage)
    }

    const onBrowseImage = (e) => {
        if (curId !== "") {
            setName(document.getElementById('input-name').value)
            setEmail(document.getElementById('input-email').value)
        }
        setMyImage(e.target.files[0]);
        setBrowsedImage(URL.createObjectURL(e.target.files[0]))
        console.log("onBrowse",browsedImage);
    }

    return (
        <div>
            <Toast show={toastShow} onClose={toggle} delay={10000} autohide
                style={{
                    position: 'absolute',
                    top: 0,
                    right: 0,
                }}
            >
                <Toast.Header>
                    <img src="holder.js/20x20?text=%20" className="rounded mr-2" alt="" />
                    <strong className="mr-auto">Post Article</strong>
                    <small>just now</small>
                </Toast.Header>
                <Toast.Body>{message}</Toast.Body>
            </Toast>
            <h2>{strings.author}</h2>
            <Row>
                <Col sm={9}>
                    <Form>
                        <Form.Group className="mb-3" >
                            <Form.Label>{strings.author}</Form.Label>
                            <Form.Control type="text" placeholder={strings.author} name="name" id="input-name"
                                onChange={(e) => onTextChange(e)}
                            />
                            <Form.Text className="text-danger" id="name">
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" >
                            <Form.Label>{strings.email}</Form.Label>
                            <Form.Control type="text" placeholder={strings.email} name="email" id="input-email"
                                onChange={(e) => onTextChange(e)}
                            />
                            <Form.Text className="text-danger" id="email">
                            </Form.Text>
                        </Form.Group>
                        <Button variant="primary" disabled={!isSave}
                            onClick={onSave}
                        >Save</Button>
                    </Form>
                </Col>
                <Col sm={3}>
                    <label htmlFor="image-choose">
                        <img src={browsedImage ? browsedImage : '../../images/placeholder.jpg'} className="w-100" />
                    </label>
                    <input type="file" name="" id="image-choose" style={{ display: 'none' }}
                        onChange={(e) => onBrowseImage(e)}
                    />
                    <p>{browsedImage === "" ? 'Import Image' : ''}</p>
                </Col>
            </Row><br />
            <Row>
                <Col>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{strings.name}</th>
                                <th>{strings.email}</th>
                                <th>{strings.photo}</th>
                                <th>{strings.action}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                state.map((item, index) => (

                                    <tr key={index}>
                                        <td>{item._id.slice(0, 8)}</td>
                                        <td>{item.name}</td>
                                        <td>{item.email}</td>
                                        <td><img src={item.image} alt="" style={{ height: '60px' }} /></td>
                                        <td>
                                            <Button variant="warning" onClick={() => onSelect(item,index)}>Edit</Button>{` `}
                                            <Button variant="danger" onClick={() => onDelete(item._id)}>Delete</Button>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </div>
    )
}
