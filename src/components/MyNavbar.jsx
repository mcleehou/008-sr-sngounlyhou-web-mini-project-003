import React, { useContext } from 'react'
import {
    Navbar, Container, Nav, FormControl,
    NavDropdown, Form, Button
} from 'react-bootstrap'
import {Link } from 'react-router-dom'
import { strings } from '../localization/language'
import { LangContext } from '../util/langContext'

export default function MyNavbar() {

    const context = useContext(LangContext)

    const onChangeLanguage = (lang)=>{
        context.setLang(lang)
        strings.setLanguage(lang)
        localStorage.setItem('lang',lang)
      }
    return (
        <div>
            <Navbar bg="primary" expand="lg" variant="dark" className="mb-3">
                <Container>
                    <Navbar.Brand href="#">AMS Redux</Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse id="navbarScroll">
                        <Nav
                            className="me-auto my-2 my-lg-0"
                            style={{ maxHeight: '100px' }}
                        >
                            <Nav.Link as={ Link } to={'/'}>{strings.home}</Nav.Link>
                            <Nav.Link as={ Link } to={'/article'}>{strings.article}</Nav.Link>
                            <Nav.Link as={ Link } to={'/author'}>{strings.author}</Nav.Link>
                            <Nav.Link as={ Link } to={'/category'}>{strings.category}</Nav.Link>
                            <NavDropdown title={strings.language} id="navbarScrollingDropdown">
                                <NavDropdown.Item onClick={()=>onChangeLanguage('kh')}>ខ្មែរ</NavDropdown.Item>
                                <NavDropdown.Item onClick={()=>onChangeLanguage('en')}>English</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                        <Form className="d-flex">
                            <FormControl
                                type="search"
                                placeholder="Search"
                                className="mr-2"
                                aria-label="Search"
                            />
                            <Button variant="light">Search</Button>
                        </Form>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}
