import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { API } from '../util/url'

export default function MyView() {

    const [article, setArticle] = useState({})

    let {id} = useParams()
 
    useEffect( async () => {
        let result = await API.get('/articles/'+id)
        setArticle(result.data.data)
    }, [])
    return (
        <div>
            <h3>Title: <span>{article.title}</span> </h3>
            <img style={{ width: '300px' }} src={article.image} alt="" />
            <h3>Description: <span>{article.description}</span></h3>
        </div>
    )
}
