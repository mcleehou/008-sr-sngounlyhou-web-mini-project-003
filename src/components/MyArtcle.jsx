import React, { useEffect, useState } from 'react'
import { strings } from '../localization/language'
import { Row, Col, Form, Button, Toast } from 'react-bootstrap'
import { uploadImage, fetchAddArticle, fetchEditArticle } from '../service/amsService'
import { useDispatch, useSelector } from 'react-redux'
import { fetchAllAuthor } from '../redux/actions/articleAction'
import { useParams } from 'react-router-dom'
import { API } from '../util/url'

export default function MyArtcle() {

    const [title, setTitle] = useState(" ")
    const [desc, setDesc] = useState(" ")
    const [isButtonSave, setIsButtonSave] = useState(false)
    const [MyImage, setMyImage] = useState("")
    const [browsedImage, setBrowsedImage] = useState("")
    const [message, setMessage] = useState("")
    const [toastShow, setToastShow] = useState(false)

    const toggle = () => setToastShow(!toastShow)
    const [article, setArticle] = useState({})
    // const [curId, setCurId] = useState("")
    let { id } = useParams()

    const dispatch = useDispatch()
    const state = useSelector(state => state)


    useEffect(async () => {
        dispatch(fetchAllAuthor())
        if (id !== undefined) {
            let result = await API.get('/articles/' + id)
            setArticle(result.data.data)
            setBrowsedImage(result.data.data.image)
            document.getElementById('title').value = result.data.data.title
            document.getElementById('desc').value = result.data.data.description

        }
    }, [])

    useEffect(() => {
        if (title === "" || title === " " || desc === "" || desc === " ")
            setIsButtonSave(false)
        else setIsButtonSave(true)

        if (title === "")
            document.getElementById('home-title').innerText = "Title Cannot be Empty!"
        else document.getElementById('home-title').innerText = ""

        if (desc === "")
            document.getElementById('home-desc').innerText = "Description Cannot be Empty!"
        else document.getElementById('home-desc').innerText = ""
    })


    let onTextChange = (even) => {
        if (id !== undefined) {
            setTitle(document.getElementById('title').value)
            setDesc(document.getElementById('desc').value)
        }
        if (even.name === "title") {
            setTitle(even.value)
        }
        else {
            setDesc(even.value)
        }
    }

    const onBrowseImage = (e) => {
        if (id !== undefined) {
            setTitle(document.getElementById('title').value)
            setDesc(document.getElementById('desc').value)
        }
        setMyImage(e.target.files[0]);
        setBrowsedImage(URL.createObjectURL(e.target.files[0]))
    }

    const onSave = async () => {
        if (id === undefined) {
            const url = MyImage && await uploadImage(MyImage)
            let article = {
                title: title,
                description: desc,
                image: url
            }
            let res = await fetchAddArticle(article)
            setMessage(res.data.message)
            document.getElementById('title').value = ""
            document.getElementById('desc').value = ""
            setBrowsedImage("")
        }
        else {
            const url = MyImage && await uploadImage(MyImage)
            let article = {
                title: title,
                description: desc,
                image: url ? url : browsedImage
            }
            let res = await fetchEditArticle(article, id)
            setMessage(res.data.message)
        }
        setToastShow(true)
        setIsButtonSave(false)
    }
    return (
        <div>
            <Toast show={toastShow} onClose={toggle} delay={10000} autohide
                style={{
                    position: 'absolute',
                    top: 0,
                    right: 0,
                }}
            >
                <Toast.Header>
                    <img src="holder.js/20x20?text=%20" className="rounded mr-2" alt="" />
                    <strong className="mr-auto">Post Article</strong>
                    <small>just now</small>
                </Toast.Header>
                <Toast.Body>{message}</Toast.Body>
            </Toast>
            <h2>{id === undefined ? strings.addArticle : strings.editArticle}</h2>

            <Row>
                <Col sm={8}>
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Label>{strings.title}</Form.Label>
                            <Form.Control type="text" placeholder={strings.title}
                                name="title" id="title"
                                onChange={(e) => onTextChange(e.target)}
                            />
                            <Form.Text className="text-danger" id="home-title">
                            </Form.Text>
                        </Form.Group>
                        <Form.Group controlId="home-author">
                            <Form.Label>{strings.author}</Form.Label>
                            <Form.Control as="select">
                                {
                                    state.author.map((item, index) => (
                                        <option key={index} value={item._id}>{item.name}</option>
                                    ))
                                }

                            </Form.Control>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>{strings.description}</Form.Label>
                            <Form.Control as="textarea" rows={3} placeholder={strings.description}
                                name="desc" id="desc"
                                onChange={(e) => onTextChange(e.target)}
                            />
                            <Form.Text className="text-danger" id="home-desc"></Form.Text>
                        </Form.Group>
                        <Button variant="primary" disabled={!isButtonSave}
                            onClick={onSave}
                        >{id === undefined ? 'Post' : 'save'}</Button>
                    </Form>
                </Col>
                <Col sm={4}>
                    <label htmlFor="image-choose">
                        <img src={browsedImage ? browsedImage : '../../images/placeholder.jpg'} className="w-100" />
                    </label>
                    <input type="file" name="" id="image-choose" style={{ display: 'none' }}
                        onChange={(e) => onBrowseImage(e)}
                    />
                    <p>{browsedImage === "" ? 'Import Image' : ''}</p>
                </Col>
            </Row>


        </div>
    )
}

