import LocalizedStrings from 'react-localization';

export const strings = new LocalizedStrings({
 en:{
   home:"Home",
   category:"Category",
   article:"Article",
   author:"Author",
   language:"Language",
   addArticle:"Add Article",
   editArticle:"Edit Article",
   title:"Title",
   password:"Password",
   description:"Description",
   name:"Name",
   email:"Email",
   photo:"Photo",
   action:"Action"
   
 },
 kh: {
   home:"ទំព័រដើម",
   category:"ប្រភេទអត្ថបទ",
   article:"អត្ថបទ",
   author:"អ្នកនិពន្ធ",
   language:"ភាសា",
   addArticle:"បន្ថែមអត្ថបទ",
   editArticle:"កែប្រែអត្ថបទ",
   title:"ចំណងជើង",
   password:"ពាក្យសម្ងាត់",
   description:"ការពិពណ៍នា",
   name:"ឈ្មោះ",
   email:"អ៊ីម៊ែល",
   photo:"រូបភាព",
   action:"សកម្មភាព"
   
 }
});