import React from 'react';
import ReactDOM from 'react-dom'
import App from './App.js'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Provider } from 'react-redux';
import { store } from './redux/store/store.js'
import './index.css'



ReactDOM.render(
  <div >
    <Provider store={store}>
      
        <App/>
      
    </Provider>
  </div>,
  document.getElementById('root')
);
