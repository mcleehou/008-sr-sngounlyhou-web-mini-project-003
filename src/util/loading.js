import React from 'react';
import ReactLoading from 'react-loading';
 
const loading = ( type, color ) => (
    <ReactLoading type={type} color={color} height={66} width={100} 
    // style={{CaretPosition: 'absolute', top: '50%', left: '50%'}}
    />
);
 
export default loading