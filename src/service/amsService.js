import { API } from "../util/url";

export const uploadImage = async (image) =>{
    try {
        const fd = new FormData();
        fd.append("image", image)
        const result = await API.post("/images", fd)
        return result.data.url
       } catch (error) {
           console.log("uploadImage Error:", error);
       }
}

export const fetchAddArticle = async (article) =>{
    let response = await API.post('/articles',article)
    return response
}

export const fetchAuthorById = async (idx) => {
    let response = await API.get(`/author/${idx}`)
    return ({
        payload: response.data
    })
}

export const fetchEditArticle = async (article,idx) => {
    let response = await API.patch(`/articles/${idx}`+ article)
    return ({
        payload: response.data
    })
}
