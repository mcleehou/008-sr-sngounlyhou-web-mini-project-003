import React, { useEffect, useState } from 'react'
import MyNavbar from './components/MyNavbar'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import MyHome from './components/MyHome'
import MyArtcle from './components/MyArtcle'
import { Container } from 'react-bootstrap'
import MyAuthor from './components/MyAuthor'
import MyCategory from './components/MyCategory'
import { LangContext } from './util/langContext'
import { strings } from './localization/language'
import Switch from 'react-bootstrap/esm/Switch'
import MyView from './components/MyView'

export default function App() {

    const [lang, setLang] = useState()

    useEffect(() => {
        strings.setLanguage(localStorage.getItem('lang'))
        setLang(localStorage.getItem('lang'))
    }, [])

    return (
        <div>
            <Router>
                <LangContext.Provider value={{ lang, setLang }}>

                    <MyNavbar />
                    <Container>
                        <Switch>
                        <Route exact path="/" component={MyHome} />
                        <Route path="/view/:id" component={MyView}/>
                        <Route path="/edit/:id" component={MyArtcle}/>
                        <Route path="/article" component={MyArtcle} />
                        <Route path="/author" component={MyAuthor} />
                        <Route path="/category" component={MyCategory} />
                        </Switch>
                    </Container>
                </LangContext.Provider>
            </Router>

        </div>
    )
}
