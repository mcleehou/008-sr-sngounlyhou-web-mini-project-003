import { API } from "../../util/url"
import {  
    FETCH_ADD_AUTHOR, 
    FETCH_ALL_ARTICLE, 
    FETCH_ALL_AUTHOR, 
    FETCH_ALL_CATEGORY, 
    FETCH_DELETE_ARTICLE,
    FETCH_DELETE_AUTHOR,
    FETCH_EDIT_AUTHOR
} from "../constanst/actionConstanst"


export const fetchAllArticle = (page) => async dp => {
    let response = await API.get(`/articles?page=${page}&size=2`)
    return dp({
        type: FETCH_ALL_ARTICLE,
        payload: response.data
    })
}

export const fetchAllCategories = () => async dp =>{
    let response = await API.get('/category')
    return dp({
        type: FETCH_ALL_CATEGORY,
        payload: response.data
    })
}

export const deleteArticle = (idx) => async dp =>{
    let response = await API.delete(`/articles/${idx}`)
    return dp({
        type: FETCH_DELETE_ARTICLE,
        payload: response
    })
}

export const fetchAllAuthor = () => async dp => {
    let response = await API.get('/author')
    return dp({
        type: FETCH_ALL_AUTHOR,
        payload: response.data
    })
}

export const fetchAddAuthor = (author) => async dp =>{
    let response = await API.post('/author',author)
    return dp({
        type: FETCH_ADD_AUTHOR,
        payload: response.data
    })
}

export const fetchEditAuthor = (author,index,idx) => async dp =>{
    let response = await API.put(`/author/${idx}`,author)
    return dp({
        type: FETCH_EDIT_AUTHOR,
        index: index,
        payload: response.data
    })
}

export const deleteAuthor = (idx) => async dp =>{
    let response = await API.delete(`/author/${idx}`)
    return dp({
        type: FETCH_DELETE_AUTHOR,
        payload: response
    })
}

