import { applyMiddleware, createStore } from "redux";
import thunk from 'redux-thunk'
import { articleReducer } from "../reducer/reducer";

export const store = createStore(articleReducer,applyMiddleware(thunk))