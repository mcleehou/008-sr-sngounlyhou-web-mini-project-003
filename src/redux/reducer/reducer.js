import { 
    FETCH_ADD_AUTHOR, 
    FETCH_ALL_ARTICLE, 
    FETCH_ALL_AUTHOR, 
    FETCH_ALL_CATEGORY, 
    FETCH_DELETE_ARTICLE,
    FETCH_DELETE_AUTHOR,
    FETCH_EDIT_AUTHOR
} from "../constanst/actionConstanst"

const initialState = {
    article:[],
    category:[],
    author:[],
    totalPage: null,
}

export const articleReducer = (state = initialState, { type, index, payload }) => {
    switch (type) {

    case FETCH_ALL_ARTICLE:
        return { ...state, article:[...payload.data],totalPage:payload.total_page }

    case FETCH_ALL_CATEGORY:
        return { ...state, category:[...payload.data] }

    case FETCH_DELETE_ARTICLE:
        return { ...state, article: state.article.filter(item => item._id !== payload.data.data._id) }
    
    case FETCH_ALL_AUTHOR:
        return { ...state, author:[...payload.data] }

    case FETCH_ADD_AUTHOR:
        state.author.unshift(payload.data)
        return { ...state, author: [...state.author] }
    
    case FETCH_EDIT_AUTHOR:
        console.log("Before: ", state.author[index]);
        console.log("Payload Data: ", payload.data);
        return { ...state, author: [...state.author,state.author[index]= payload.data] }

    case FETCH_DELETE_AUTHOR:
        return { ...state, author: state.author.filter(item => item._id !== payload.data.data._id) }

    default:
        return state
    }
}
