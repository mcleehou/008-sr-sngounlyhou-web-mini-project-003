export const FETCH_ALL_ARTICLE = "FETCH_ALL_ARTICLE"
export const FETCH_ADD_ARTICLE = "FETCH_ADD_ARTICLE"
export const FETCH_DELETE_ARTICLE = "FETCH_DELETE_ARTICLE"
export const FETCH_ALL_CATEGORY = "FETCH_ALL_CATEGORY"
export const FETCH_ALL_AUTHOR = "FETCH_ALL_AUTHOR"
export const FETCH_AUTHOR_BY_ID = "FETCH_AUTHOR_BY_ID"
export const FETCH_ADD_AUTHOR = "FETCH_ADD_AUTHOR"
export const FETCH_EDIT_AUTHOR = "FETCH_EDIT_AUTHOR"
export const FETCH_DELETE_AUTHOR = "FETCH_DELETE_AUTHOR"
